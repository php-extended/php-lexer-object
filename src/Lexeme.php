<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

/**
 * Lexeme class file.
 * 
 * This class represents a simple element that was lexed by the lexer.
 * 
 * @author Anastaszor
 */
class Lexeme implements LexemeInterface
{
	
	/**
	 * The code of the lexeme.
	 * 
	 * @var integer
	 */
	protected int $_code;
	
	/**
	 * The line at the start of the lexeme.
	 * 
	 * @var integer
	 */
	protected int $_line;
	
	/**
	 * The column at the start of the lexeme.
	 * 
	 * @var integer
	 */
	protected int $_column;
	
	/**
	 * The data of the lexeme.
	 * 
	 * @var string
	 */
	protected string $_data;
	
	/**
	 * Builds a new lexeme with the given code, position in the stream and data.
	 * 
	 * @param integer $code
	 * @param integer $line
	 * @param integer $column
	 * @param string $data
	 */
	public function __construct(int $code, int $line, int $column, string $data)
	{
		$this->_code = $code;
		$this->_line = $line;
		$this->_column = $column;
		$this->_data = $data;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'Code : '.\str_pad((string) $this->_code, 3, ' ', \STR_PAD_LEFT)
			."\tLine : ".\str_pad((string) $this->_line, 3, ' ', \STR_PAD_LEFT)
			."\tColumn : ".\str_pad((string) $this->_column, 3, ' ', \STR_PAD_LEFT)
			."\tData : ".$this->_data;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexemeInterface::getCode()
	 */
	public function getCode() : int
	{
		return $this->_code;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexemeInterface::getLine()
	 */
	public function getLine() : int
	{
		return $this->_line;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexemeInterface::getColumn()
	 */
	public function getColumn() : int
	{
		return $this->_column;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexemeInterface::getData()
	 */
	public function getData() : string
	{
		return $this->_data;
	}
	
}
