<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

use RuntimeException;
use SplFileObject;

/**
 * FileObjectLexer class file.
 * 
 * This class is a lexer based on a spl file object as data source.
 * 
 * @author Anastaszor
 */
class FileObjectLexer extends AbstractLexer implements LexerInterface
{
	
	/**
	 * The file object.
	 * 
	 * @var SplFileObject
	 */
	protected SplFileObject $_fileObject;
	
	/**
	 * @param SplFileObject $fobj
	 * @param ?LexerConfigurationInterface $config
	 */
	public function __construct(SplFileObject $fobj, ?LexerConfigurationInterface $config = null)
	{
		parent::__construct($config);
		$this->_fileObject = $fobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\AbstractLexer::rewind()
	 * @throws RuntimeException
	 */
	public function rewind() : void
	{
		$this->_fileObject->rewind();
		parent::rewind();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\AbstractLexer::getNextChar()
	 */
	protected function getNextChar() : ?string
	{
		$res = $this->_fileObject->fread(1);
		if(false === $res || '' === $res)
		{
			return null;
		}
		
		return $res;
	}
	
}
