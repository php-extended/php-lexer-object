<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

/**
 * LexerMappingRule class file.
 * 
 * This class is a simple implementation of the LexerMappingRuleInterface.
 * 
 * @author Anastaszor
 */
class LexerMappingRule implements LexerMappingRuleInterface
{
	
	/**
	 * The value of the lexeme.
	 * 
	 * @var string
	 */
	protected string $_value;
	
	/**
	 * The code of the lexeme.
	 * 
	 * @var integer
	 */
	protected int $_code;
	
	/**
	 * Builds a new LexerMappingRule with the given value and code.
	 * 
	 * @param string $value
	 * @param integer $code
	 */
	public function __construct(string $value, int $code)
	{
		$this->_value = $value;
		$this->_code = $code;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerMappingRuleInterface::getValue()
	 */
	public function getValue() : string
	{
		return $this->_value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerMappingRuleInterface::getCode()
	 */
	public function getCode() : int
	{
		return $this->_code;
	}
	
}
