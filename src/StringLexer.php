<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

/**
 * StringLexer class file.
 * 
 * This class is a lexer based on a string as data source.
 * 
 * @author Anastaszor
 */
class StringLexer extends AbstractLexer implements LexerInterface
{
	
	/**
	 * The string data.
	 * 
	 * @var string
	 */
	protected string $_string;
	
	/**
	 * The maximum length on the string.
	 * 
	 * @var integer
	 */
	protected int $_len = 0;
	
	/**
	 * The current index on the string.
	 * 
	 * @var integer
	 */
	protected int $_cur = 0;
	
	/**
	 * Builds a new StringLexer with the given string and configuration.
	 * 
	 * @param string $data
	 * @param ?LexerConfigurationInterface $config
	 */
	public function __construct(string $data, ?LexerConfigurationInterface $config = null)
	{
		parent::__construct($config);
		$this->_string = $data;
		$this->_len = (int) \mb_strlen($data, '8bit');
		$this->_cur = 0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\AbstractLexer::rewind()
	 */
	public function rewind() : void
	{
		$this->_cur = 0;
		parent::rewind();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\AbstractLexer::getNextChar()
	 */
	protected function getNextChar() : ?string
	{
		if($this->_cur >= $this->_len)
		{
			return null;
		}
		
		return $this->_string[$this->_cur++];
	}
	
}
