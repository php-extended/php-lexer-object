<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

use PhpExtended\Io\InputStreamInterface;
use Psr\Http\Message\StreamInterface;
use SplFileObject;

/**
 * LexerFactory class file.
 * 
 * This class is a simple implementation of the LexerFactoryInterface.
 * 
 * @author Anastaszor
 */
class LexerFactory implements LexerFactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerFactoryInterface::createFromString()
	 */
	public function createFromString(string $data, ?LexerConfigurationInterface $config = null) : LexerInterface
	{
		return new StringLexer($data, $config);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerFactoryInterface::createFromFileObject()
	 */
	public function createFromFileObject(SplFileObject $file, ?LexerConfigurationInterface $config = null) : LexerInterface
	{
		return new FileObjectLexer($file, $config);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerFactoryInterface::createFromStream()
	 */
	public function createFromStream(StreamInterface $stream, ?LexerConfigurationInterface $config = null) : LexerInterface
	{
		return new StreamLexer($stream, $config);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerFactoryInterface::createFromInput()
	 */
	public function createFromInput(InputStreamInterface $stream, ?LexerConfigurationInterface $config = null) : LexerInterface
	{
		return new InputStreamLexer($stream, $config);
	}
	
}
