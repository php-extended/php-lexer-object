<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

/**
 * CodeFilterLexer class file.
 * 
 * This class is a lexer that rejects certains lexemes based on their codes.
 * By default, this rejects lexemes with the Trash code, as they are non-valid
 * semantically in the parsed data.
 * 
 * @author Anastaszor
 */
class CodeFilterLexer implements LexerInterface
{
	
	/**
	 * The inner lexer.
	 * 
	 * @var LexerInterface
	 */
	protected LexerInterface $_inner;
	
	/**
	 * The current lexeme.
	 * 
	 * @var ?LexemeInterface
	 */
	protected ?LexemeInterface $_current = null;
	
	/**
	 * The codes that are rejected.
	 * 
	 * @var array<integer, integer>
	 */
	protected array $_rejected = [];
	
	/**
	 * The current key.
	 * 
	 * @var integer
	 */
	protected int $_key = 0;
	
	/**
	 * Builds a new CodeFilterIterator based on the given lexer and the codes
	 * that are rejected in the stream.
	 * @param LexerInterface $lexer
	 * @param array<integer, integer> $rejected
	 */
	public function __construct(LexerInterface $lexer, array $rejected = [LexerInterface::L_TRASH])
	{
		$this->_inner = $lexer;
		$this->_rejected = $rejected;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerInterface::current()
	 * @psalm-suppress InvalidNullableReturnType
	 * @psalm-suppress NullableReturnStatement
	 */
	public function current() : LexemeInterface
	{
		/** @phpstan-ignore-next-line */
		return $this->_current;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		$this->_current = null;
		if($this->_inner->valid())
		{
			do
			{
				$current = $this->_inner->current();
				$this->_inner->next();
				if(!\in_array($current->getCode(), $this->_rejected, true))
				{
					$this->_current = $current;
					break;
				}
			}
			while($this->_inner->valid());
		}
		
		if(null === $this->_current)
		{
			$this->_current = new Lexeme(LexerInterface::L_EOS, 0, 0, '');
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return $this->_key;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return null === $this->_current || LexerInterface::L_EOS !== $this->_current->getCode();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		$this->_key = 0;
		$this->_inner->rewind();
		$this->next();
	}
	
}
