<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

/**
 * LexerMergingRule class file.
 * 
 * This class is a simple implementation of the LexerMergingRuleInterface.
 * 
 * @author Anastaszor
 */
class LexerMergingRule implements LexerMergingRuleInterface
{
	
	/**
	 * The code of the before part.
	 * 
	 * @var integer
	 */
	protected int $_beforeCode;
	
	/**
	 * The code of the after part.
	 * 
	 * @var integer
	 */
	protected int $_afterCode;
	
	/**
	 * The new code.
	 * 
	 * @var integer
	 */
	protected int $_newCode;
	
	/**
	 * Builds a new LexerMergingRule with the given codes.
	 * 
	 * @param integer $before
	 * @param integer $after
	 * @param integer $new
	 */
	public function __construct(int $before, int $after, int $new)
	{
		$this->_beforeCode = $before;
		$this->_afterCode = $after;
		$this->_newCode = $new;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerMergingRuleInterface::getBeforeCode()
	 */
	public function getBeforeCode() : int
	{
		return $this->_beforeCode;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerMergingRuleInterface::getAfterCode()
	 */
	public function getAfterCode() : int
	{
		return $this->_afterCode;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerMergingRuleInterface::getNewCode()
	 */
	public function getNewCode() : int
	{
		return $this->_newCode;
	}
	
}
