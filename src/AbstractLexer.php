<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

use PhpExtended\Charset\CharacterSetInterface;

/**
 * AbstractLexer class file.
 * 
 * This class is a lexer that creates lexemes based on a char-by-char analysis
 * provided by an underlying input stream of characters.
 * 
 * @author Anastaszor
 */
abstract class AbstractLexer implements LexerInterface
{
	
	/**
	 * The expected charset of the input stream.
	 * 
	 * @var CharacterSetInterface
	 */
	protected CharacterSetInterface $_charset;
	
	/**
	 * The codes of the lexemes depending of the encountered chars.
	 * 
	 * @var array<string, integer>
	 */
	protected array $_mappings = [];
	
	/**
	 * The codes of the new lexemes depending of the old ones.
	 * 
	 * @var array<integer, array<integer, integer>>
	 */
	protected array $_mergings = [];
	
	/**
	 * The current lexeme.
	 * 
	 * @var ?LexemeInterface
	 */
	protected ?LexemeInterface $_currentLexeme = null;
	
	/**
	 * The current lexeme count.
	 *
	 * @var integer
	 */
	protected int $_key = 0;
	
	/**
	 * The current line count.
	 *
	 * @var integer
	 */
	protected int $_line = 1;
	
	/**
	 * The current column count.
	 *
	 * @var integer
	 */
	protected int $_col = 0;
	
	/**
	 * The code for the next lexeme.
	 * 
	 * @var ?integer
	 */
	protected ?int $_nextCode = null;
	
	/**
	 * The line of the next value.
	 * 
	 * @var integer
	 */
	protected int $_nextLine = 1;
	
	/**
	 * The column of the next value.
	 * 
	 * @var integer
	 */
	protected int $_nextCol = 0;
	
	/**
	 * The character that begins the next lexeme.
	 * 
	 * @var ?string
	 */
	protected ?string $_nextChar = null;
	
	/**
	 * Builds a new Lexer with the given configuration.
	 * 
	 * @param ?LexerConfigurationInterface $config
	 */
	public function __construct(?LexerConfigurationInterface $config = null)
	{
		if(null === $config)
		{
			return;
		}
		$this->_charset = $config->getCharset();
		
		foreach($config->getMappingRules() as $mappingRule)
		{
			// @var $mappingRule \PhpExtended\Lexer\LexerMappingRuleInterface
			$this->_mappings[$mappingRule->getValue()] = $mappingRule->getCode();
		}
		
		foreach($config->getMergingRules() as $mergingRule)
		{
			// @var $mergingRule \PhpExtended\Lexer\LexerMergingRuleInterface
			$this->_mergings[$mergingRule->getBeforeCode()][$mergingRule->getAfterCode()] = $mergingRule->getNewCode();
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the next character from the underlying stream.
	 * 
	 * @return ?string
	 */
	abstract protected function getNextChar() : ?string;
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerInterface::current()
	 */
	public function current() : LexemeInterface
	{
		return $this->_currentLexeme ?? new Lexeme(LexerInterface::L_EOS, $this->_line, $this->_col, '');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		$this->_currentLexeme = null;
		/** @var ?int $currentCode */
		$currentCode = $this->_nextCode;
		$currentLine = $this->_line;
		$currentColumn = $this->_col;
		$currentChars = $this->_nextChar ?? '';
		
		do
		{
			$this->_line = $this->_nextLine;
			$this->_col = $this->_nextCol;
			$char = $this->getNextChar() ?? '';
			$charCode = $this->getCharCode($char);
			
			// EOS cannot be merged
			if(LexerInterface::L_EOS === $charCode)
			{
				$this->_nextCode = LexerInterface::L_EOS;
				$this->_nextChar = '';
				break;
			}
			
			if(null === $currentCode)
			{
				$currentCode = $charCode;
				$currentChars .= $char;
				continue;
			}
			
			$this->handleColumnLineIncrementation($char);
			
			$mergedCode = $this->_mergings[$currentCode][$charCode] ?? $this->_mergings[$currentCode][LexerInterface::L_EVERYTHING_ELSE] ?? null;
			if(null === $mergedCode)
			{
				// we have found the first char of the next token
				// do not merge it, then release
				$this->_nextCode = $charCode;
				$this->_nextChar = $char;
				break;
			}
			
			// else we have found a new merged token
			$currentCode = $mergedCode;
			$currentChars .= $char;
		}
		while(LexerInterface::L_EOS !== $currentCode);
		
		$this->_key++;
		$this->_currentLexeme = new Lexeme($currentCode ?? LexerInterface::L_EOS, $currentLine, $currentColumn, $currentChars);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return $this->_key;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return null !== $this->_currentLexeme && LexerInterface::L_EOS !== $this->_currentLexeme->getCode();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		$this->_line = 1;
		$this->_nextLine = 1;
		$this->_col = 0;
		$this->_nextCol = 1;
		$this->_key = -1;
		$this->next();
	}
	
	/**
	 * Gets a suitable char code from the configuration mappings.
	 * 
	 * @param string $char
	 * @return int the char code
	 */
	protected function getCharCode(string $char) : int
	{
		if('' === $char)
		{
			return LexerInterface::L_EOS;
		}
		
		return $this->_mappings[$char] ?? LexerInterface::L_TRASH;
	}
	
	/**
	 * Handles the column and line incrementations.
	 * 
	 * @param string $char
	 */
	protected function handleColumnLineIncrementation(string $char) : void
	{
		$this->_nextCol++;
		if("\n" === $char)
		{
			$this->_nextLine++;
			$this->_nextCol = 0;
		}
	}
	
}
