<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

use PhpExtended\Io\InputStreamInterface;

/**
 * InputStreamLexer class file.
 * 
 * This class is a based on an input stream as data source.
 * 
 * @author Anastaszor
 */
class InputStreamLexer extends AbstractLexer implements LexerInterface
{
	
	/**
	 * The input stream as data source.
	 * 
	 * @var InputStreamInterface
	 */
	protected InputStreamInterface $_inputStream;
	
	/**
	 * The current char to be read.
	 * 
	 * @var ?string
	 */
	protected ?string $_currentChar;
	
	/**
	 * Builds a new InputStreamLexer class file.
	 * 
	 * @param InputStreamInterface $stream
	 * @param ?LexerConfigurationInterface $config
	 */
	public function __construct(InputStreamInterface $stream, ?LexerConfigurationInterface $config = null)
	{
		parent::__construct($config);
		$this->_inputStream = $stream;
		$this->_currentChar = $this->_inputStream->read(1);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\AbstractLexer::getNextChar()
	 */
	protected function getNextChar() : ?string
	{
		if(null === $this->_currentChar || '' === $this->_currentChar)
		{
			return null;
		}
		
		$cur = $this->_currentChar;
		$this->_currentChar = $this->_inputStream->read(1);
		
		return $cur;
	}
	
}
