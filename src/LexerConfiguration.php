<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

use ArrayIterator;
use Iterator;
use PhpExtended\Charset\CharacterSetInterface;
use PhpExtended\Charset\CharacterSetReference;
use PhpExtended\Charset\UTF_8;

/**
 * LexerConfiguration class file.
 * 
 * This class represents a configuration to build a lexer.
 * 
 * @author Anastaszor
 */
class LexerConfiguration implements LexerConfigurationInterface
{
	
	/**
	 * The character set used.
	 * 
	 * @var CharacterSetInterface
	 */
	protected CharacterSetInterface $_charset;
	
	/**
	 * The mapping rules.
	 * 
	 * @var array<integer, LexerMappingRuleInterface>
	 */
	protected array $_mappings = [];
	
	/**
	 * The merging rules.
	 * 
	 * @var array<integer, LexerMergingRuleInterface>
	 */
	protected array $_mergings = [];
	
	/**
	 * Builds a new LexerConfiguration with the default rules.
	 * 
	 * @param ?CharacterSetInterface $charset
	 * @psalm-suppress PossiblyNullPropertyAssignmentValue
	 */
	public function __construct(?CharacterSetInterface $charset = null)
	{
		// merge trash together (trash + trash => trash)
		$this->_mergings[] = new LexerMergingRule(LexerInterface::L_TRASH, LexerInterface::L_TRASH, LexerInterface::L_TRASH);
		if(null === $charset)
		{
			$charsetReference = new CharacterSetReference();
			$charset = $charsetReference->lookup(UTF_8::class);
		}
		/** @phpstan-ignore-next-line */
		$this->_charset = $charset;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerConfigurationInterface::setCharset()
	 */
	public function setCharset(CharacterSetInterface $charset) : LexerConfigurationInterface
	{
		$this->_charset = $charset;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerConfigurationInterface::getCharset()
	 */
	public function getCharset() : CharacterSetInterface
	{
		return $this->_charset;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerConfigurationInterface::addMappings()
	 */
	public function addMappings(string $string, int $code) : LexerConfigurationInterface
	{
		for($i = 0; \mb_strlen($string, '8bit') > $i; $i++)
		{
			$this->_mappings[] = new LexerMappingRule($string[$i], $code);
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerConfigurationInterface::getMappingRules()
	 */
	public function getMappingRules() : Iterator
	{
		return new ArrayIterator($this->_mappings);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerConfigurationInterface::addMerging()
	 */
	public function addMerging(int $before, int $after, int $new) : LexerConfigurationInterface
	{
		$this->_mergings[] = new LexerMergingRule($before, $after, $new);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerConfigurationInterface::getMergingRules()
	 */
	public function getMergingRules() : Iterator
	{
		return new ArrayIterator($this->_mergings);
	}
	
}
