<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

use Psr\Http\Message\StreamInterface;
use RuntimeException;

/**
 * StreamLexer class file.
 * 
 * This class is a lexer based on a stream as data source.
 * 
 * @author Anastaszor
 */
class StreamLexer extends AbstractLexer implements LexerInterface
{
	
	/**
	 * The stream data source.
	 * 
	 * @var StreamInterface
	 */
	protected StreamInterface $_stream;
	
	/**
	 * Builds a new StreamInterface with the given.
	 * 
	 * @param StreamInterface $stream
	 * @param ?LexerConfigurationInterface $config
	 */
	public function __construct(StreamInterface $stream, ?LexerConfigurationInterface $config = null)
	{
		parent::__construct($config);
		$this->_stream = $stream;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\AbstractLexer::rewind()
	 * @throws RuntimeException
	 */
	public function rewind() : void
	{
		$this->_stream->rewind();
		parent::rewind();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\AbstractLexer::getNextChar()
	 */
	protected function getNextChar() : ?string
	{
		try
		{
			$ret = $this->_stream->read(1);
			if('' === $ret)
			{
				return null;
			}
		}
		// @codeCoverageIgnoreStart
		catch(RuntimeException $r)
		{
			$ret = null;
		}
		// @codeCoverageIgnoreEnd
		
		return $ret;
	}
	
}
