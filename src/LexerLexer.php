<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Lexer;

/**
 * LexerLexer class file.
 * 
 * This class is a consolidation lexer based on a previous lexer.
 * 
 * @author Anastaszor
 */
class LexerLexer implements LexerInterface
{
	
	/**
	 * The previous lexer.
	 * 
	 * @var LexerInterface
	 */
	protected LexerInterface $_previous;
	
	/**
	 * The merging rules.
	 * 
	 * @var array<integer, array<integer, integer>>
	 */
	protected array $_mergings = [];
	
	/**
	 * The current lexeme.
	 * 
	 * @var ?LexemeInterface
	 */
	protected ?LexemeInterface $_currentLexeme = null;
	
	/**
	 * The next lexeme.
	 * 
	 * @var ?LexemeInterface
	 */
	protected ?LexemeInterface $_nextLexeme = null;
	
	/**
	 * The current key.
	 * 
	 * @var integer
	 */
	protected int $_key = 0;
	
	/**
	 * Builds a new LexerLexer with the given previous lexer and configuration.
	 * 
	 * @param LexerInterface $previous
	 * @param ?LexerConfigurationInterface $config
	 */
	public function __construct(LexerInterface $previous, ?LexerConfigurationInterface $config = null)
	{
		$this->_previous = $previous;
		if(null !== $config)
		{
			/** @var LexerMergingRuleInterface $mergingRule */
			foreach($config->getMergingRules() as $mergingRule)
			{
				$this->_mergings[$mergingRule->getBeforeCode()][$mergingRule->getAfterCode()] = $mergingRule->getNewCode();
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Lexer\LexerInterface::current()
	 * @psalm-suppress InvalidNullableReturnType
	 * @psalm-suppress NullableReturnStatement
	 */
	public function current() : LexemeInterface
	{
		/** @phpstan-ignore-next-line */
		return $this->_currentLexeme;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		if(null !== $this->_currentLexeme && LexerInterface::L_EOS === $this->_currentLexeme->getCode())
		{
			return;
		}
		
		$this->_currentLexeme = $this->_nextLexeme;
		$this->_nextLexeme = null;
		if($this->_previous->valid())
		{
			do
			{
				$lexeme = $this->_previous->current();
				$this->_previous->next();
				if(LexerInterface::L_EOS === $lexeme->getCode())
				{
					// EOS does not merge with any other lexeme
					// should not happen as if current is EOS, early break
					// @codeCoverageIgnoreStart
					$this->_nextLexeme = $lexeme;
					break;
					// @codeCoverageIgnoreEnd
				}
				
				if(null === $this->_currentLexeme)
				{
					$this->_currentLexeme = $lexeme;
					continue;
				}
				
				$merged = $this->mergeLexeme($this->_currentLexeme, $lexeme);
				if(null !== $merged)
				{
					$this->_currentLexeme = $merged;
					continue;
				}
				
				$this->_nextLexeme = $lexeme;
				break;
			}
			while($this->_previous->valid());
		}
		
		if(null === $this->_currentLexeme)
		{
			$this->_currentLexeme = new Lexeme(LexerInterface::L_EOS, 0, 0, '');
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return $this->_key;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return null === $this->_currentLexeme || LexerInterface::L_EOS !== $this->_currentLexeme->getCode();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		$this->_key = 0;
		$this->_previous->rewind();
		$this->next();
	}
	
	/**
	 * Gets a merged lexeme if both given lexeme are mergeable, null else.
	 * 
	 * @param LexemeInterface $before
	 * @param LexemeInterface $after
	 * @return null|LexemeInterface
	 */
	protected function mergeLexeme(LexemeInterface $before, LexemeInterface $after)
	{
		if(isset($this->_mergings[$before->getCode()][$after->getCode()]))
		{
			return new Lexeme(
				$this->_mergings[$before->getCode()][$after->getCode()],
				$before->getLine(),
				$before->getColumn(),
				$before->getData().$after->getData(),
			);
		}
		
		return null;
	}
	
}
