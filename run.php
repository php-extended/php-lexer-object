<?php declare(strict_types=1);

/**
 * Test script for the lexer.
 * 
 * @author Anastaszor
 */

use PhpExtended\Lexer\LexerFactory;
use PhpExtended\Lexer\LexerConfiguration;
use PhpExtended\Lexer\CodeFilterLexer;
use PhpExtended\Lexer\LexerLexer;
use PhpExtended\Lexer\LexerInterface;

global $argv;

if(!isset($argv[1]))
{
	throw new RuntimeException('The first argument should be the data to parse.');
}
$data = $argv[1];

$autoload = __DIR__.'/vendor/autoload.php';
if(!is_file($autoload))
{
	throw new \RuntimeException('Composer must be runned first.');
}
require $autoload;

echo 'Input : '."\n";
echo "\t".$data;
echo "\n\n";

class CONSTS {
	const DIGIT = 1;
	const DOT   = 2;
	const DIGITWDOT = 3;
	const DIGITWTRAIL = 4;
}

$factory = new LexerFactory();
$config = new LexerConfiguration();
$config->addMappings(LexerInterface::CLASS_DIGIT, CONSTS::DIGIT);
// parses all dots as class dot
$config->addMappings('.', CONSTS::DOT);
// a digit followed by a digit is still a digit
$config->addMerging(CONSTS::DIGIT, CONSTS::DIGIT, CONSTS::DIGIT);
// a digit followed by a dot is a digit-dot complex
$config->addMerging(CONSTS::DIGIT, CONSTS::DOT, CONSTS::DIGITWDOT);
// a digit-dot complex followed by a dot is a digit-with-trail complex
$config->addMerging(CONSTS::DIGITWDOT, CONSTS::DIGIT, CONSTS::DIGITWTRAIL);
// a digit-with-trail complex followed by a digit is still a digit-with-trail
$config->addMerging(CONSTS::DIGITWTRAIL, CONSTS::DIGIT, CONSTS::DIGITWTRAIL);

$lexer = $factory->createFromString($data, $config);
foreach($lexer as $lexeme)
{
	echo "\t".$lexeme."\n";
}
echo "\n\n";

$lexer = new CodeFilterLexer($factory->createFromString($data, $config));
foreach($lexer as $lexeme)
{
	echo "\t".$lexeme."\n";
}
echo "\n\n";

$lexer = new LexerLexer(new CodeFilterLexer($factory->createFromString($data, $config)), $config);
foreach($lexer as $lexeme)
{
	echo "\t".$lexeme."\n";
}
