<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Io\InputStreamInterface;
use PhpExtended\Lexer\InputStreamLexer;
use PhpExtended\Lexer\Lexeme;
use PhpExtended\Lexer\LexerInterface;
use PHPUnit\Framework\TestCase;

class ProxyInputStream implements InputStreamInterface
{
	
	private string $_data;
	
	private int $_current = 0;
	
	public function __construct(string $data)
	{
		$this->_data = $data;
		$this->_current = 0;
	}
	
	public function __toString() : string {}
	
	public function read(int $numberOfBytes) : ?string
	{
		$r = \substr($this->_data, $this->_current, $numberOfBytes);
		$this->_current += $numberOfBytes;
		
		return $r;
	}
	
	public function readline() : ?string {}
	
}

/**
 * InputStreamLexerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Lexer\AbstractLexer
 * @covers \PhpExtended\Lexer\InputStreamLexer
 *
 * @internal
 *
 * @small
 */
class InputStreamLexerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var InputStreamLexer
	 */
	protected InputStreamLexer $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$expected = [
			new Lexeme(LexerInterface::L_TRASH, 1, 0, 'd'),
			new Lexeme(LexerInterface::L_TRASH, 1, 1, 'a'),
			new Lexeme(LexerInterface::L_TRASH, 1, 2, 't'),
			new Lexeme(LexerInterface::L_TRASH, 1, 3, 'a'),
		];
		
		$actual = [];
		
		foreach($this->_object as $lexeme)
		{
			$actual[] = $lexeme;
		}
		
		$this->assertEquals($expected, $actual);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new InputStreamLexer(new ProxyInputStream('data'));
	}
	
}
