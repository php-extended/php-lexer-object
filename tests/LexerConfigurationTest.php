<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Charset\CharacterSetReference;
use PhpExtended\Charset\ISO_8859_1;
use PhpExtended\Charset\UTF_8;
use PhpExtended\Lexer\LexerConfiguration;
use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Lexer\LexerMappingRule;
use PhpExtended\Lexer\LexerMergingRule;
use PHPUnit\Framework\TestCase;

/**
 * LexerConfigurationTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Lexer\LexerConfiguration
 *
 * @internal
 *
 * @small
 */
class LexerConfigurationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LexerConfiguration
	 */
	protected LexerConfiguration $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetCharset() : void
	{
		$this->assertInstanceOf(UTF_8::class, $this->_object->getCharset());
		$this->_object->setCharset((new CharacterSetReference())->lookup(ISO_8859_1::class));
		$this->assertInstanceOf(ISO_8859_1::class, $this->_object->getCharset());
	}
	
	public function testGetMappings() : void
	{
		$this->assertEquals(new ArrayIterator([]), $this->_object->getMappingRules());
		$this->_object->addMappings('abc', 1);
		$expected = new ArrayIterator([
			new LexerMappingRule('a', 1),
			new LexerMappingRule('b', 1),
			new LexerMappingRule('c', 1),
		]);
		$this->assertEquals($expected, $this->_object->getMappingRules());
	}
	
	public function testGetMergings() : void
	{
		$this->assertEquals(new ArrayIterator([
			new LexerMergingRule(LexerInterface::L_TRASH, LexerInterface::L_TRASH, LexerInterface::L_TRASH),
		]), $this->_object->getMergingRules());
		$this->_object->addMerging(1, 2, 3);
		$this->_object->addMerging(2, 3, 4);
		$expected = new ArrayIterator([
			new LexerMergingRule(LexerInterface::L_TRASH, LexerInterface::L_TRASH, LexerInterface::L_TRASH),
			new LexerMergingRule(1, 2, 3),
			new LexerMergingRule(2, 3, 4),
		]);
		$this->assertEquals($expected, $this->_object->getMergingRules());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LexerConfiguration();
	}
	
}
