<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Lexer\LexerMergingRule;
use PHPUnit\Framework\TestCase;

/**
 * LexerMergingRuleTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Lexer\LexerMergingRule
 *
 * @internal
 *
 * @small
 */
class LexerMergingRuleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LexerMergingRule
	 */
	protected LexerMergingRule $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetBeforeCode() : void
	{
		$this->assertEquals(12, $this->_object->getBeforeCode());
	}
	
	public function testGetAfterCode() : void
	{
		$this->assertEquals(14, $this->_object->getAfterCode());
	}
	
	public function testGetNewCode() : void
	{
		$this->assertEquals(16, $this->_object->getNewCode());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LexerMergingRule(12, 14, 16);
	}
	
}
