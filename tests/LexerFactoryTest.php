<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Io\InputStreamInterface;
use PhpExtended\Lexer\FileObjectLexer;
use PhpExtended\Lexer\InputStreamLexer;
use PhpExtended\Lexer\LexerFactory;
use PhpExtended\Lexer\StreamLexer;
use PhpExtended\Lexer\StringLexer;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\StreamInterface;

/**
 * LexerFactoryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Lexer\LexerFactory
 *
 * @internal
 *
 * @small
 */
class LexerFactoryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LexerFactory
	 */
	protected LexerFactory $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testCreateFromString() : void
	{
		$this->assertInstanceOf(StringLexer::class, $this->_object->createFromString('data'));
	}
	
	public function testCreateFromFileObject() : void
	{
		$this->assertInstanceOf(FileObjectLexer::class, $this->_object->createFromFileObject(new SplFileObject(__FILE__)));
	}
	
	public function testCreateFromStream() : void
	{
		$this->assertInstanceOf(StreamLexer::class, $this->_object->createFromStream($this->getMockForAbstractClass(StreamInterface::class)));
	}
	
	public function testCreateFromInput() : void
	{
		$this->assertInstanceOf(InputStreamLexer::class, $this->_object->createFromInput($this->getMockForAbstractClass(InputStreamInterface::class)));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LexerFactory();
	}
	
}
