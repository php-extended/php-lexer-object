<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Lexer\Lexeme;
use PhpExtended\Lexer\LexerConfiguration;
use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Lexer\LexerLexer;
use PhpExtended\Lexer\StringLexer;
use PHPUnit\Framework\TestCase;

/**
 * LexerLexerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Lexer\LexerLexer
 *
 * @internal
 *
 * @small
 */
class LexerLexerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LexerLexer
	 */
	protected LexerLexer $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$expected = [
			new Lexeme(1, 1, 0, 'this'),
			new Lexeme(LexerInterface::L_TRASH, 1, 4, ' '),
			new Lexeme(1, 1, 5, 'is'),
			new Lexeme(LexerInterface::L_TRASH, 1, 7, ' '),
			new Lexeme(1, 1, 8, 'a'),
			new Lexeme(LexerInterface::L_TRASH, 1, 9, ' '),
			new Lexeme(1, 1, 10, 'string'),
		];
		
		$actual = [];
		
		foreach($this->_object as $key => $lexeme)
		{
			$actual[] = $lexeme;
			$this->assertIsInt($key);
		}
		
		$this->assertEquals($expected, $actual);
		$this->_object->next();
		$final = $this->_object->current();
		$this->assertEquals(LexerInterface::L_EOS, $final->getCode());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$strConfiguration = new LexerConfiguration();
		$strConfiguration->addMappings(LexerInterface::CLASS_ALNUM, 1);
		$lexConfiguration = new LexerConfiguration();
		$lexConfiguration->addMerging(1, 1, 1);
		$this->_object = new LexerLexer(new StringLexer('this is a string', $strConfiguration), $lexConfiguration);
	}
	
}
