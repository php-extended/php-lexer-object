<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Lexer\Lexeme;
use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Lexer\StreamLexer;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\StreamInterface;

class ProxyStream implements StreamInterface
{
	private $_data;
	
	public function __construct(string $data)
	{
		$this->_data = \fopen('php://memory', 'rw');
		\fwrite($this->_data, $data);
	}
	
	public function __toString() : string {}
	
	public function getMetadata(?string $key = null) : void {}
	
	public function isSeekable() : bool {}
	
	public function read(int $length) : string
	{
		return \fread($this->_data, $length);
	}
	
	public function tell() : int {}
	
	public function isWritable() : bool {}
	
	public function seek(int $offset, int $whence = \SEEK_SET) : void {}
	
	public function getSize() : ?int {}
	
	public function rewind() : void
	{
		\rewind($this->_data);
	}
	
	public function detach() : void {}
	
	public function getContents() : string {}
	
	public function close() : void {}
	
	public function eof() : bool
	{
		return \feof($this->_data);
	}
	
	public function write(string $string) : int {}
	
	public function isReadable() : bool {}
	
}

/**
 * StreamLexerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Lexer\AbstractLexer
 * @covers \PhpExtended\Lexer\StreamLexer
 *
 * @internal
 *
 * @small
 */
class StreamLexerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StreamLexer
	 */
	protected StreamLexer $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$expected = [
			new Lexeme(LexerInterface::L_TRASH, 1, 0, 'd'),
			new Lexeme(LexerInterface::L_TRASH, 1, 1, 'a'),
			new Lexeme(LexerInterface::L_TRASH, 1, 2, 't'),
			new Lexeme(LexerInterface::L_TRASH, 1, 3, 'a'),
		];
		
		$actual = [];
		
		foreach($this->_object as $lexeme)
		{
			$actual[] = $lexeme;
		}
		
		$this->assertEquals($expected, $actual);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StreamLexer(new ProxyStream('data'));
	}
	
}
