<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Lexer\LexerMappingRule;
use PHPUnit\Framework\TestCase;

/**
 * LexerMappingRuleTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Lexer\LexerMappingRule
 *
 * @internal
 *
 * @small
 */
class LexerMappingRuleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LexerMappingRule
	 */
	protected LexerMappingRule $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetValue() : void
	{
		$this->assertEquals('abcdef', $this->_object->getValue());
	}
	
	public function testGetCode() : void
	{
		$this->assertEquals(12, $this->_object->getCode());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LexerMappingRule('abcdef', 12);
	}
	
}
