<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Lexer\Lexeme;
use PHPUnit\Framework\TestCase;

/**
 * LexemeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Lexer\Lexeme
 *
 * @internal
 *
 * @small
 */
class LexemeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Lexeme
	 */
	protected Lexeme $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('Code :  12	Line :  14	Column :  16	Data : data', $this->_object->__toString());
	}
	
	public function testGetCode() : void
	{
		$this->assertEquals(12, $this->_object->getCode());
	}
	
	public function testGetLine() : void
	{
		$this->assertEquals(14, $this->_object->getLine());
	}
	
	public function testGetColumn() : void
	{
		$this->assertEquals(16, $this->_object->getColumn());
	}
	
	public function testGetData() : void
	{
		$this->assertEquals('data', $this->_object->getData());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Lexeme(12, 14, 16, 'data');
	}
	
}
