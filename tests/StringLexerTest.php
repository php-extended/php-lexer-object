<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Lexer\Lexeme;
use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Lexer\StringLexer;
use PHPUnit\Framework\TestCase;

/**
 * StringLexerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Lexer\AbstractLexer
 * @covers \PhpExtended\Lexer\StringLexer
 *
 * @internal
 *
 * @small
 */
class StringLexerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StringLexer
	 */
	protected StringLexer $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$expected = [
			new Lexeme(LexerInterface::L_TRASH, 1, 0, 'd'),
			new Lexeme(LexerInterface::L_TRASH, 1, 1, 'a'),
			new Lexeme(LexerInterface::L_TRASH, 1, 2, 't'),
			new Lexeme(LexerInterface::L_TRASH, 1, 3, 'a'),
		];
		
		$actual = [];
		
		foreach($this->_object as $lexeme)
		{
			$actual[] = $lexeme;
		}
		
		$this->assertEquals($expected, $actual);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StringLexer('data');
	}
	
}
