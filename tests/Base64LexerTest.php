<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Lexer\AbstractLexer;
use PhpExtended\Lexer\LexerConfiguration;
use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Lexer\StringLexer;
use PHPUnit\Framework\TestCase;

/**
 * AbstractLexerTest test file.
 *
 * @author Anastaszor
 * @covers \PhpExtended\Lexer\AbstractLexer
 *
 * @internal
 *
 * @small
 */
class Base64LexerTest extends TestCase
{
	
	/**
	 * The object to test.
	 *
	 * @var AbstractLexer
	 */
	protected AbstractLexer $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$count = 0;
		
		foreach($this->_object as $token)
		{
			$this->assertEquals(1, $token->getCode());
			$count++;
		}
		
		$this->assertEquals(1, $count);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$configuration = new LexerConfiguration();
		$configuration->addMappings(LexerInterface::CLASS_ALNUM, 1);
		$configuration->addMappings('/+=', 1); // other characters in base64 string
		$configuration->addMerging(1, 1, 1); // merge letters into words
		
		$data = \random_bytes(20);
		
		for($i = 0; 45 > $i; $i++)
		{
			$data = \base64_encode($data);
		}
		
		// $data about ~ 10Mo string (10667800 bytes)
		
		$this->_object = new StringLexer($data, $configuration);
	}
	
}
