<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Lexer\FileObjectLexer;
use PhpExtended\Lexer\Lexeme;
use PhpExtended\Lexer\LexerInterface;
use PHPUnit\Framework\TestCase;

/**
 * FileObjectLexerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Lexer\AbstractLexer
 * @covers \PhpExtended\Lexer\FileObjectLexer
 *
 * @internal
 *
 * @small
 */
class FileObjectLexerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var FileObjectLexer
	 */
	protected FileObjectLexer $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$expected = [
			new Lexeme(LexerInterface::L_TRASH, 1, 0, 'd'),
			new Lexeme(LexerInterface::L_TRASH, 1, 1, 'a'),
			new Lexeme(LexerInterface::L_TRASH, 1, 2, 't'),
			new Lexeme(LexerInterface::L_TRASH, 1, 3, 'a'),
		];
		
		$actual = [];
		
		foreach($this->_object as $lexeme)
		{
			$actual[] = $lexeme;
		}
		
		$this->assertEquals($expected, $actual);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new FileObjectLexer(new SplFileObject(__DIR__.'/FileObjectFile.txt'));
	}
	
}
