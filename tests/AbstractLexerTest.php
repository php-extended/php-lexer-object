<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-lexer-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Lexer\AbstractLexer;
use PhpExtended\Lexer\Lexeme;
use PhpExtended\Lexer\LexerConfiguration;
use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Lexer\StringLexer;
use PHPUnit\Framework\TestCase;

/**
 * AbstractLexerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Lexer\AbstractLexer
 *
 * @internal
 *
 * @small
 */
class AbstractLexerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AbstractLexer
	 */
	protected AbstractLexer $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$expected = [
			new Lexeme(1, 1, 0, 'this'),
			new Lexeme(LexerInterface::L_TRASH, 1, 4, ' '),
			new Lexeme(1, 1, 5, 'is'),
			new Lexeme(LexerInterface::L_TRASH, 1, 7, "\n"),
			new Lexeme(1, 2, 0, 'the'),
			new Lexeme(LexerInterface::L_TRASH, 2, 3, ' '),
			new Lexeme(1, 2, 4, 'data'),
		];
		
		$actual = [];
		
		foreach($this->_object as $key => $lexeme)
		{
			$actual[$key] = $lexeme;
		}
		
		$this->assertEquals($expected, $actual);
		
		$this->_object->next();
		$this->_object->next();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$configuration = new LexerConfiguration();
		$configuration->addMappings(LexerInterface::CLASS_ALPHA, 1);
		$configuration->addMerging(1, 1, 1); // merge letters into words
		$this->_object = new StringLexer("this is\nthe data", $configuration);
	}
	
}
